function [stats,results,run_stats] = targetedNGS(pathToRef,pathToReads,verbose)
%TARGETEDNGS Map and analyze targeted NGS reads
% USAGE
%   [stats,results,run_stats] = targetedNGSwin(pathToRef,pathToReads)
% INPUT
%   pathToRef   Full path to reference FASTA [char]
%   pathToReads Full path to folder containing paired-end reads [char]
%   verbose     Verbosity; 0=none, 1=terminal, 2=graphical [double]
% OUTPUT
%   stats       Full mapping statistics by sample and reference [table]
%   results     Positive and indeterminate calls [table]
%   run_stats   Run statistics assuming pathToReads is full run [table]
% WRITES
%   graphs      Folder containing one graph per sample and run stats graph
%   mapped      Folder containing mapping BAM and BAM index files
%   results     Folder containing all OUTPUTs written to CSV files
%   unpaired    Folder containing all oprhaned FASTQs (no mate found)
% NOTES
%   (1) Requires bwa.exe, samtools.exe, and cygwin DLLs for Windows. DLLs
%   must be in the same location as the executables to run properly. Both
%   executables must also be on the MATLAB PATH to be seen.
%   (2) Requires bwa and samtools executables for Unix. Can be either
%   compiled executables on system PATH or installed packages.
% COPYRIGHT
%   Copyright (C) 2016, Turner Conrad

%#ok<*AGROW>
%#ok<*NASGU>
%#ok<*WNOFF>
%#ok<*WNON>

%% INPUT

% Record start time
timestamp = strrep(strrep(char(datetime),':','-'),' ','_');

% Remove warnings
warning('off');

% Check verbosity
if nargin < 3 % no verbose value provided, default to graphical
    verbose = 2;
elseif ~isnumeric(verbose) && ~isscalar(verbose) && (verbose == 0 || verbose == 1 || verbose == 2)
    displayerror('Verbosity must be 0=none, 1=command, or 2=graphical!',[],1);
end

% Verify or retrieve reads path
if nargin < 2 || isempty(pathToReads)
    pathToReads = uigetdir('','Select folder containing reads');
elseif ~exist(pathToReads,'dir')
    displayerror('Provided reads path cannot be located!',[],1);
end

% Make subdirectories
if ~exist([pathToReads filesep 'mapped'],'dir')
    mkdir([pathToReads filesep 'mapped']);
end
if ~exist([pathToReads filesep 'graphs'],'dir')
    mkdir([pathToReads filesep 'graphs']);
end
if ~exist([pathToReads filesep 'results'],'dir')
    mkdir([pathToReads filesep 'results']);
end

% Initialize log file
log_fi_path = [pathToReads filesep 'results' filesep 'Log_' timestamp '.txt'];
log_fi = fopen(log_fi_path,'w');

% Set default cleanup to all objects
cleanobj = onCleanup(@() eval('close all hidden'));

% Display analysis start
msg = sprintf('%s [TARGNGS] Starting targeted NGS analysis\n',datestr(now));
fprintf(log_fi,msg);
if verbose == 1; fprintf('\n%s',msg); end;

% Retrieve reference
if nargin < 1 || isempty(pathToRef)
    [ref_file,ref_path] = uigetfile({'*.fa';'*.fasta';'*.fna';'*.fa';'*.seq';'*.fsa';'*.ffn''*.txt'},'Select reference');
    pathToRef = [ref_path filesep ref_file];
elseif ~exist(pathToRef,'file')
    displayerror('Provided reference file not found!',log_fi,verbose);
end

% Attempt to open reference file
try
    ref = fastaread(pathToRef);
catch ME
    displayerror('Could not read reference FASTA!',log_fi,verbose);
end

% Gunzip FASTQs
tic;
gz = getByExt(pathToReads,'.fastq.gz',log_fi,verbose);
gz = gz(cellfun(@(x) isempty(strfind(x,'Undetermined')),gz)); % skip undetermined reads
gz = cellfun(@(x) [pathToReads filesep x],gz,'UniformOutput',false);
if ~isempty(gz) % gzipped FASTQs found
    fprintf(log_fi,'%s [TARGNGS] Gunzipping reads:\n',datestr(now));
    if verbose == 1
        fprintf('%s [TARGNGS] Gunzipping reads:\n0%% |%s| 100%%\n   |',datestr(now),repmat('=',1,length(gz)));
    elseif verbose == 2
        wb = waitbar(0,'Gunzipping reads');
    end
    % Serially gunzip FASTQs with system or MATLAB
    for i = 1:length(gz)
        % Attemp gunzip and produce soft error if fails
        try % to gunzip
            if isunix
                system(sprintf('gunzip "%s"',gz{i}));
            else
                gunzip(gz{i});
            end
            fprintf(log_fi,'%s [TARGNGS] Successfully gunzipped %s\n',datestr(now),gz{i});
        catch ME % error in gunzipping
            fprintf(log_fi,'%s [SOFTERR] Failed to gunzip %s\n',datestr(now),gz{i});
        end
        % Fill in status bar
        if verbose == 1
            fprintf('='); % update terminal status bar
        elseif verbose == 2
            waitbar(i/length(gz)); % update waitbar
        end
    end
    % Display step completion
    if verbose == 1
        fprintf('| Completed in %0.2f min\n',toc/60);
    elseif verbose == 2 && ishandle(wb)
        close(wb);
    elseif verbose == 2 && ~ishandle(wb)
        % user exited graphical waitbar, do not continue gunzipping
        return;
    end
end

% Survey all available fastq files
fastq = getByExt(pathToReads,'.fastq',log_fi,verbose);
fastq = fastq(cellfun(@(x) isempty(strfind(x,'Undetermined')),fastq)); % skip undetermined again
if isempty(fastq) % none available
    displayerror('No FASTQ files found in provided reads path!',log_fi,verbose);
end

% Ensure paired only, triage unpaired to separate folder
r1 = fastq(cellfun(@(x) ~isempty(strfind(x,'_R1_')),fastq)); % only R1s
r2 = fastq(cellfun(@(x) ~isempty(strfind(x,'_R2_')),fastq)); % only R2s
for orphan = [setdiff(r1,strrep(r2,'_R2_','_R1_')); setdiff(r2,strrep(r1,'_R1_','_R2_'))]'
    % Make unpaired orphans folder if not already there
    if ~exist([pathToReads filesep 'unpaired'],'dir')
        mkdir([pathToReads filesep 'unpaired']);
    end
    fprintf(log_fi,'%s [SOFTERR] Moved orphaned FASTQ %s to unpaired folder\n',datestr(now),orphan{1});
    % Move orphan into unpaired folder
    movefile([pathToReads filesep orphan{1}],[pathToReads filesep 'unpaired' filesep orphan{1}]);
end
fastq = [r1 strrep(r1,'_R1_','_R2_')]; % redefine FASTQs by matched R1s

% Generate sample names and fill in BAM and FASTQ full paths
samples = cellfun(@(x) {regexprep(x,'(.gz)|(.bz2)|(.fastq)|(.fq)|(_L001_.*)','')},fastq(:,1));
bam = cellfun(@(x) {[pathToReads filesep 'mapped' filesep x '.bam']},samples);
fastq = [cellfun(@(x) [pathToReads filesep x],fastq(:,1),'UniformOutput',false) cellfun(@(x) [pathToReads filesep x],fastq(:,2),'UniformOutput',false)];

% Check for BWA
if ismac
    bwa_loc = '/usr/local/bin/bwa'; % default homebrew path
    if ~exist(bwa_loc,'file')
        displayerror('BWA not found in /usr/local/bin directory!',log_fi,verbose);
    end
elseif isunix
    [stat,~] = system('which bwa');
    if stat % unsucessful
        displayerror('BWA not found on system PATH!',log_fi,verbose);
    else % no full path needed, just name
        bwa_loc = 'bwa';
    end
elseif ispc
    bwa_loc = which('bwa.exe');
    if isempty(bwa_loc)
        displayerror('BWA not found on the MATLAB PATH!',log_fi,verbose);
    end
else
    displayerror('Unsupported OS detected during search for BWA executable!',log_fi,verbose);
end

% Check for Samtools
if ismac
    samtools_loc = '/usr/local/bin/samtools'; % default homebrew path
    if ~exist(samtools_loc,'file')
        displayerror('Samtools not found in /usr/local/bin directory!',log_fi,verbose);
    end
elseif isunix
    [stat,~] = system('which samtools');
    if stat % unsucessful
        displayerror('Samtools not found on system PATH!',log_fi,verbose);
    else % no full path needed, just name
        samtools_loc = 'samtools';
    end
elseif ispc
    samtools_loc = which('samtools.exe');
    if isempty(bwa_loc)
        displayerror('Samtools not found on the MATLAB path!',log_fi,verbose);
    end
else
    displayerror('Unsupported OS detected during search for Samtools executable!',log_fi,verbose);
end

% Check for index and do so if not found
if ~isempty(find(cellfun(@(x) ~exist([pathToRef x],'file'),{'.amb' '.ann' '.bwt' '.pac' '.sa'}),1)) % all potential index files
    [stat,~] = system(sprintf('"%s" index "%s"',bwa_loc,pathToRef));
    if stat
        displayerror('Reference could not be indexed by BWA!',log_fi,verbose);
    end
end

% Display initial information
msg = sprintf('%s [TARGNGS] Reference: %s\n',datestr(now),pathToRef);
msg = sprintf('%s%s [TARGNGS] Reads Path: %s\n',msg,datestr(now),pathToReads);
msg = sprintf('%s%s [TARGNGS] Number of Samples: %d\n',msg,datestr(now),length(samples));
fprintf(log_fi,msg);
if verbose == 1; fprintf(msg); end;

%% PROCESS

% Ask to overwrite files if verbosity is graphical
if verbose == 2 && ~isempty(find(cellfun(@(x) exist(x,'file'),bam),1))
    overwrite_choice = questdlg('Mapped reads are detected. Would you like to overwrite these files?','Overwrite mapped reads','Yes','No','Exit','No');
    switch overwrite_choice
        case 'Yes'
            overwrite = true;
        case 'No'
            overwrite = false;
        case 'Exit'
            return;
        otherwise
            overwrite = false;
    end
    fprintf(log_fi,'%s [TARGNGS] User selected %s as BAM overwrite option\n',datestr(now),overwrite_choice);
elseif verbose ~= 2
    overwrite = false;
end

% Map reads
fprintf(log_fi,'%s [TARGNGS] Mapping reads\n',datestr(now));
if verbose == 1 % initialize status bar
    fprintf('%s [TARGNGS] Mapping reads:\n0%% |%s| 100%% (= success, * mapped reads available, ! error)\n   |',datestr(now),repmat('=',1,length(samples)));
elseif verbose == 2 % initialize waitbar
    wb = waitbar(0,'Mapping reads ...');
end
failedMap = cell(length(samples),2); % record of failed mappings and reasons
% Serially map each sample
tic;
for i = 1:length(samples)
    if verbose == 2
        waitbar((i-1)/length(samples),wb);
    end
    % Get FASTQ and BAM information ready for size check later
    r1_size = dir(fastq{i,1});
    r2_size = dir(fastq{i,2});
    bam_size.bytes = -1;
    if exist(bam{i},'file')
        bam_size = dir(bam{i});
    end
    % Check to ensure that FASTQ has reads for mapping
    if (isfield(r1_size,'bytes') && (r1_size.bytes == 0)) || (isfield(r2_size,'bytes') && (r2_size.bytes == 0))
        failedMap(i,1) = bam(i);
        failedMap(i,2) = {'REASON: No reads'};
        fprintf('!');
        fprintf(log_fi,'%s [SOFTERR] Sample %s has no reads to map!\n',datestr(now),samples{i});
    elseif ~exist(bam{i},'file') || (exist(bam{i},'file') && overwrite) || (exist(bam{i},'file') && bam_size.bytes == 0) % no BAM, BAM to overwrite, or empty BAM
        % Set pipeline command    
        pipecmd = sprintf([ ...
            '"%s" mem -t %d ' ... % bwa_loc, threads
            '-R "@RG:\tID:%s\tCN:USAMRIID_DSD\t' ...
                'DS:TARGETED_SEQUENCING\tPL:ILLUMINA\t' ...
                'PM:MISEQ\tSM:%s" ' ... % sample, sample
            '"%s" "%s" "%s" | ' ... % reference, mate 1, mate 2
            '"%s" sort -@ %d | ' ... % samtools_loc, threads
            '"%s" calmd -eb --reference "%s" - > ' ... % samtools_loc
            '"%s"'], ... % reference, bam
            bwa_loc,feature('numCores')*2,samples{i},samples{i}, ...
            pathToRef,fastq{i,1},fastq{i,2},samtools_loc, ...
            feature('numCores')*2,samtools_loc,pathToRef,bam{i});
        fprintf(log_fi,'%s [TARGNGS] Running pipe: %s\n',datestr(now),pipecmd);
        % Execute pipeline and record outcome
        [stat,cmdout] = system(pipecmd);
        if stat % failed
            failedMap(i,1) = bam(i);
            failedMap(i,2) = {['REASON: Pipe error: ' cmdout]};
            fprintf(log_fi,'%s [SOFTERR] Pipe error on sample %s: %s\n',datestr(now),samples{i},cmdout);
            if verbose == 1
                fprintf('!');
            end
        else % success
            bam_size = dir(bam{i});
            if isfield(bam_size,'bytes') && bam_size.bytes > 0 % BAM has reads
                fprintf(log_fi,'%s [TARGNGS] Sample %s successfully mapped\n',datestr(now),samples{i});
                if verbose == 1
                    fprintf('=');
                end
            else % empty BAM
                failedMap(i,1) = bam(i);
                failedMap(i,2) = {'REASON: No reads in BAM'};
                fprintf(log_fi,'%s [SOFTERR] No reads were mapped for %s\n',datestr(now),samples{i});
                if verbose == 1
                    fprintf('!');
                end
            end
        end
    else
        fprintf(log_fi,'%s [TARGNSG] Mapped reads BAM found for %s\n',datestr(now),samples{i});
        if verbose == 1
            % update status bar with pre-mapped symbol
            fprintf('*');
        end
    end
    if verbose == 2 && ~ishandle(wb)
        % user exited graphical waitbar, do not continue mapping
        return;
    end
end
if verbose == 1
    fprintf('| Completed in %0.2f min\n',toc/60);
elseif verbose == 2 && ishandle(wb)
    close(wb);
elseif verbose == 2 && ~ishandle(wb)
    % user exited graphical waitbar, do not continue
    return;
end

% Remove failed mappings
ind = cellfun(@(x) ~isempty(x),failedMap(:,1));
if ~isempty(find(ind,1)) % at least one failed
    fprintf('Failed to map the following samples:\n');
    disp(failedMap(ind,:));
    bam = bam(~ind);
    samples = samples(~ind);
end

% Initialize parallel pool if not already
fprintf(log_fi,'%s [TARGNGS] Initializing parallel pool\n',datestr(now));
if verbose == 2
    wb = waitbar(0,'Starting parallel pool ...');
end
gcp;
if verbose == 2 && ishandle(wb)
    close(wb);
elseif verbose == 2 && ~ishandle(wb)
    % user exited graphical waitbar, do not continue
    return;
end

% Gather mapping statistics
master_stats = cell(length(samples),1);
log_tmp = cell(length(samples),1); % record of log info during parfor
run_stats = cell(length(samples),1);
fprintf(log_fi,'%s [TARGNGS] Gathering mapping statistics\n',datestr(now));
if verbose == 1
    fprintf('%s [TARGNGS] Gathering mapping statistics ... ',datestr(now));
elseif verbose == 2
    wb = waitbar(0,'Gathering mapping statistics ...');
end
tic;
% Parallel open each BAM and collection information
parfor i = 1:length(samples) % each sample
    tmp_stats = table;
    run_stats{i} = table;
    % Load reads in BioMap object and record prefilter stats
    try
        bmap = BioMap(bam{i});
        warning('off');
        stats_row = matlab.lang.makeValidName(samples{i});
        run_stats{i}{stats_row,'Prefilter_Reads'} = bmap.NSeqs;
        run_stats{i}{stats_row,'Unpaired_Reads'} = length(find(filterByFlag(bmap,'unmappedQuery',true) & filterByFlag(bmap,'unmappedMate',true)));
        run_stats{i}{stats_row,'Percent_Unpaired_Reads'} = run_stats{i}{stats_row,'Unpaired_Reads'} / run_stats{i}{stats_row,'Prefilter_Reads'};
        run_stats{i}{stats_row,'Mapped_Singlets'} = length(find(filterByFlag(bmap,'pairedInMap',false)));
        run_stats{i}{stats_row,'Percent_Mapped_Singlets'} = run_stats{i}{stats_row,'Mapped_Singlets'} / run_stats{i}{stats_row,'Prefilter_Reads'};
        % Filter undesired reads and record postfilter stats
        bmap = getSubset(bmap,filterByFlag(bmap,'pairedInSeq',true,'pairedInMap',true,'unmappedQuery',false,'unmappedMate',false,'failedQualCheck',false));
        run_stats{i}{stats_row,'Postfilter_Reads'} = bmap.NSeqs;
        run_stats{i}{stats_row,'Discarded_Reads'} = run_stats{i}{stats_row,'Prefilter_Reads'} - run_stats{i}{stats_row,'Postfilter_Reads'};
        run_stats{i}{stats_row,'Discarded_Percent'} = run_stats{i}{stats_row,'Discarded_Reads'} / run_stats{i}{stats_row,'Prefilter_Reads'};
        % Generate reference-based mapping statistics
        for ii = 1:length(ref) % each reference in sample
            warning('off');
            % Slice reads by reference
            if ~isempty(bmap.SequenceDictionary)
                bmap_sub = getSubset(bmap,'SelectReference',ii);
            else % no postfilter reads, force to record zeros in stats table
                bmap_sub = struct('NSeqs',0);
            end
            row = [matlab.lang.makeValidName(samples{i}) '_' matlab.lang.makeValidName(ref(ii).Header)];
            if bmap_sub.NSeqs == 0 % no reads mapped, default stats to 0
                tmp_stats{row,'Sample'} = {matlab.lang.makeValidName(samples{i})};
                tmp_stats{row,'Target'} = {matlab.lang.makeValidName(ref(ii).Header)};
                tmp_stats{row,'Calculation'} = 0;
                tmp_stats{row,'Depth_Mean'} = 0;
                tmp_stats{row,'Depth_RMS'} = 0;
                tmp_stats{row,'Depth_StdDev'} = 0;
                tmp_stats{row,'Depth_SNR'} = 0;
                tmp_stats{row,'Coverage'} = 0;
                tmp_stats{row,'Match_Bases'} = 0;
                tmp_stats{row,'Mismatch_Bases'} = 0;
                tmp_stats{row,'Total_Bases'} = 0;
                tmp_stats{row,'Identity'} = 0;
                tmp_stats{row,'BaseQ_Mean'} = 0;
                tmp_stats{row,'BaseQ_RMS'} = 0;
                tmp_stats{row,'BaseQ_StdDev'} = 0;
                tmp_stats{row,'BaseQ_SNR'} = 0;
                tmp_stats{row,'Match_BaseQ_Mean'} = 0;
                tmp_stats{row,'Match_BaseQ_RMS'} = 0;
                tmp_stats{row,'Match_BaseQ_StdDev'} = 0;
                tmp_stats{row,'Match_BaseQ_SNR'} = 0;
                tmp_stats{row,'Mismatch_BaseQ_Mean'} = 0;
                tmp_stats{row,'Mismatch_BaseQ_RMS'} = 0;
                tmp_stats{row,'Mismatch_BaseQ_StdDev'} = 0;
                tmp_stats{row,'Mismatch_BaseQ_SNR'} = 0;
                tmp_stats{row,'MapQ_Mean'} = 0;
                tmp_stats{row,'MapQ_RMS'} = 0;
                tmp_stats{row,'MapQ_StdDev'} = 0;
                tmp_stats{row,'MapQ_SNR'} = 0;
                tmp_stats{row,'Number_Reads'} = 0;
                tmp_stats{row,'Fraction_Reads'} = 0;
            else % has mapped reads, continue calculating
                [coverage,mapq,match_baseq,mismatch_baseq] = getMappingData(bmap_sub,ref(ii));
                tmp_stats{row,'Sample'} = {matlab.lang.makeValidName(samples{i})};
                tmp_stats{row,'Target'} = {matlab.lang.makeValidName(ref(ii).Header)};
                tmp_stats{row,'Depth_Mean'} = mean(coverage);
                tmp_stats{row,'Depth_RMS'} = rms(coverage);
                tmp_stats{row,'Depth_StdDev'} = std(coverage);
                tmp_stats{row,'Depth_SNR'} = tmp_stats{row,'Depth_Mean'} / tmp_stats{row,'Depth_StdDev'};
                tmp_stats{row,'Coverage'} = length(find(coverage)) / length(coverage);
                tmp_stats{row,'Match_Bases'} = length(match_baseq);
                tmp_stats{row,'Mismatch_Bases'} = length(mismatch_baseq);
                tmp_stats{row,'Total_Bases'} = tmp_stats{row,'Match_Bases'} + tmp_stats{row,'Mismatch_Bases'};
                tmp_stats{row,'Identity'} = tmp_stats{row,'Match_Bases'} / tmp_stats{row,'Total_Bases'};
                tmp_stats{row,'BaseQ_Mean'} = mean([match_baseq mismatch_baseq(mismatch_baseq>=0)]);
                tmp_stats{row,'BaseQ_RMS'} = rms(double([match_baseq mismatch_baseq(mismatch_baseq>=0)]));
                tmp_stats{row,'BaseQ_StdDev'} = std(double([match_baseq mismatch_baseq(mismatch_baseq>=0)]));
                tmp_stats{row,'BaseQ_SNR'} = tmp_stats{row,'BaseQ_Mean'} / tmp_stats{row,'BaseQ_StdDev'};
                tmp_stats{row,'Match_BaseQ_Mean'} = mean(match_baseq);
                tmp_stats{row,'Match_BaseQ_RMS'} = rms(double(match_baseq));
                tmp_stats{row,'Match_BaseQ_StdDev'} = std(double(match_baseq));
                tmp_stats{row,'Match_BaseQ_SNR'} = tmp_stats{row,'Match_BaseQ_Mean'} / tmp_stats{row,'Match_BaseQ_StdDev'};
                tmp_stats{row,'Mismatch_BaseQ_Mean'} = mean(mismatch_baseq(mismatch_baseq>=0));
                tmp_stats{row,'Mismatch_BaseQ_RMS'} = rms(double(mismatch_baseq(mismatch_baseq>=0)));
                tmp_stats{row,'Mismatch_BaseQ_StdDev'} = std(double(mismatch_baseq(mismatch_baseq>=0)));
                tmp_stats{row,'Mismatch_BaseQ_SNR'} = tmp_stats{row,'Mismatch_BaseQ_Mean'} / tmp_stats{row,'Mismatch_BaseQ_StdDev'};
                tmp_stats{row,'MapQ_Mean'} = mean(mapq);
                tmp_stats{row,'MapQ_RMS'} = rms(double(mapq));
                tmp_stats{row,'MapQ_StdDev'} = std(double(mapq));
                tmp_stats{row,'MapQ_SNR'} = tmp_stats{row,'MapQ_Mean'} / tmp_stats{row,'MapQ_StdDev'};
                tmp_stats{row,'Number_Reads'} = bmap_sub.NSeqs;
                tmp_stats{row,'Fraction_Reads'} = bmap_sub.NSeqs / bmap.NSeqs;
                if mean([match_baseq mismatch_baseq]) > 37
                    tmp_stats{row,'Calculation'} = tmp_stats{row,'Coverage'} * 1 * (tmp_stats{row,'MapQ_Mean'} / 60) * tmp_stats{row,'Identity'};
                else
                    tmp_stats{row,'Calculation'} = tmp_stats{row,'Coverage'} * (tmp_stats{row,'BaseQ_Mean'} / 37) * (tmp_stats{row,'MapQ_Mean'} / 60) * tmp_stats{row,'Identity'};
                end
            end
        end
        % Assigned gathered stats to master record
        master_stats{i} = tmp_stats;
        log_tmp{i} = sprintf('%s [TARGNGS] Completed analysis for %s\n',datestr(now),samples{i});
    catch ME % failed somewhere in analysis
        log_tmp{i} = sprintf('%s [SOFTERR] Could not open or process %s for analysis\n',datestr(now),bam{i});
    end
end
if verbose == 1
    fprintf('completed in %0.2f min\n',toc/60);
elseif verbose == 2 && ishandle(wb)
    close(wb);
elseif verbose == 2 && ~ishandle(wb)
    % user exited graphical waitbar, do not continue
    return;
end

% Append temporary log output to log file
log_tmp = sort(log_tmp);
for i = 1:length(log_tmp)
    fprintf(log_fi,log_tmp{i});
end

% Compile single stats table from master collection
stats = master_stats{1};
for i = 2:length(master_stats)
    stats = [stats; master_stats{i}];
end
if isempty(stats)
    displayerror('No usable statistics could be gathered from BAMS!',log_fi,verbose);
    return;
end

% Finalize run statistics
for i = 2:length(run_stats)
    run_stats{1} = [run_stats{1}; run_stats{i}];
end
run_stats = run_stats{1};
run_stats.Percent_Run = run_stats.Postfilter_Reads ./ sum(run_stats.Postfilter_Reads);

%% OUTPUT

% Write raw stats and synthesized results tables to file
fprintf(log_fi,'%s [TARGNSG] Calculating results\n',datestr(now));
if verbose == 1
    fprintf('%s [TARGNGS] Calculating results ... ',datestr(now));
elseif verbose == 2
    wb = waitbar(0,'Calculating results ...');
end
tic;
depth_cutoff = 1e3;
calc_cutoff = 0.95^4;
% Calls
stats{stats.Depth_Mean >= depth_cutoff & stats.Calculation >= calc_cutoff,'Determination'} = {'Positive'};
stats{stats.Depth_Mean >= depth_cutoff & stats.Calculation < calc_cutoff,'Determination'} = {'Indeterminate-Quality'};
stats{stats.Depth_Mean < depth_cutoff & stats.Calculation >= calc_cutoff,'Determination'} = {'Indeterminate-Depth'};
stats{stats.Depth_Mean < depth_cutoff & stats.Calculation < calc_cutoff,'Determination'} = {'Negative'};
if verbose == 2 && ishandle(wb)
    waitbar(.33);
elseif verbose == 2 && ~ishandle(wb)
    % user exited graphical waitbar, do not continue
    return;
end
% Split non-negative data points into results table
non_neg = ~ismember(stats.Determination,{'Negative'});
results = table(stats.Sample(non_neg),stats.Target(non_neg),stats.Determination(non_neg),stats.Depth_Mean(non_neg),stats.Calculation(non_neg),'VariableNames',{'Sample' 'Target' 'Determination' 'Depth' 'Quality'},'RowNames',stats.Properties.RowNames(non_neg));
results = sortrows(results,{'Determination','Depth','Quality'},{'descend','descend','descend'});
if verbose == 2 && ishandle(wb)
    waitbar(.66);
elseif verbose == 2 && ~ishandle(wb)
    % user exited graphical waitbar, do not continue
    return;
end
% Write raw and concentrated results to file
fprintf(log_fi,'%s [TARGNGS] Writing results to files\n',datestr(now));
writetable(stats,[pathToReads filesep 'results' filesep 'Mapping_statistics_' timestamp '.csv']);
writetable(results,[pathToReads filesep 'results' filesep 'Report_' timestamp '.csv']);
writetable(run_stats,[pathToReads filesep 'results' filesep 'Run_statistics_' timestamp '.csv'],'WriteRowNames',true);
save([pathToReads filesep 'results' filesep 'Results_' timestamp '.mat'],'stats','results','run_stats');
if verbose == 1
    fprintf('completed in %0.2f min\n',toc/60);
elseif verbose == 2 && ishandle(wb)
    close(wb);
elseif verbose == 2 && ~ishandle(wb)
    % user exited graphical waitbar, do not continue
    return;
end

% Create quality control plots
h = figure;
set(h,'Visible','off');
fprintf(log_fi,'%s [TARGNGS] Generating graphs\n',datestr(now));
if verbose == 1
    fprintf('%s [TARGNGS] Generating graphs ... ',datestr(now));
elseif verbose == 2
    wb = waitbar(0,'Generating graphs ...');
end
set(h,'Units','inches');
set(h,'Position',[0 0 8.5 11]);
stats_shown = {'Depth_Mean' 'Calculation' 'Coverage' 'Identity''BaseQ_Mean' 'Match_BaseQ_Mean' 'Mismatch_BaseQ_Mean' 'MapQ_Mean'};
ylims = [-1 7; 0 1.1; 0 1.1; 0 1.1; 0 40; 0 40; 0 40; 0 62];
pos = ismember(stats.Determination,{'Positive'});
ind_qual = ismember(stats.Determination,{'Indeterminate-Quality'});
ind_depth = ismember(stats.Determination,{'Indeterminate-Depth'});
neg = ismember(stats.Determination,{'Negative'}) & stats.Depth_Mean > 0 & stats.Calculation > 0.01;
for i = 1:length(stats_shown)
    % Compile data for individual statistic
    pos_dat = stats{pos,stats_shown{i}};
    ind_qual_dat = stats{ind_qual,stats_shown{i}};
    ind_depth_dat = stats{ind_depth,stats_shown{i}};
    neg_dat = stats{neg,stats_shown{i}};
    dat = NaN(max([length(pos_dat) length(ind_qual_dat) length(ind_depth_dat) length(neg_dat)]),4);
    dat(1:length(pos_dat),1) = pos_dat;
    dat(1:length(ind_qual_dat),2) = ind_qual_dat;
    dat(1:length(ind_depth_dat),3) = ind_depth_dat;
    dat(1:length(neg_dat),4) = neg_dat;
    if isequal(stats_shown{i},'Depth_Mean')
        dat = log10(dat);
        dat(isinf(dat*-1)) = 0;
    end
    % Box plot of statistic
    subplot(4,2,i);
    boxplot(dat,{'Pos','IndQ','IndD','Neg'});
    ylim(ylims(i,:));
    title(strrep(stats_shown{i},'_',' '));
end
% Print to file
set(h,'Position',[0 0 8.5 11]);
print([pathToReads filesep 'graphs' filesep 'Quality_Report.png'],'-dpng','-r300');
close(h);
if verbose == 2 && ishandle(wb)
    waitbar(length(ref)/(size(stats,1)+length(ref)));
elseif verbose == 2 && ~ishandle(wb)
    % user exited graphical waitbar, do not continue
    return;
end

% Create sample plots
scatter([],[]);
hold on;
box on;
h = gcf;
set(h,'Visible','off');
% Add labels and position
xlabel('Log_1_0 Depth');
ylabel('Quality Calculation (%)');
set(get(gca,'XLabel'),'Position',[3 -6 0]);
set(get(gca,'YLabel'),'Position',[-1.6 50 0]);
% Limit axes
xlim([-1 7]);
ylim([0 100]);
% Draw cutoff lines
line([log10(depth_cutoff) log10(depth_cutoff)],[0 100],'Color','red');
line([-1 7],[calc_cutoff*100 calc_cutoff*100],'Color','red');
% Write quadrant names
text(-0.95,77.5,'NEG','Color','red');
text(-0.95,85.4,'IND-Depth','Color',[1 0.65 0]);
text(6.95,85.4,'POS','Color','blue','HorizontalAlignment','right');
text(6.95,77.5,'IND-Qual','Color',[1 0.65 0],'HorizontalAlignment','right');
numrefs = length(ref);
for i = 1:length(ref):size(stats,1)
    % Slice stats
    stats_sub = stats(i:i+numrefs-1,:);
    title(strrep(stats_sub.Sample{1},'_',' '));
    % Draw positives and label
    check = ismember(stats_sub.Determination,{'Positive'});
    if find(check,1)
        scatter(log10(stats_sub.Depth_Mean(check)),stats_sub.Calculation(check)*100,36,'blue','filled');
        hold on;
        text(log10(stats_sub.Depth_Mean(check))+0.1,stats_sub.Calculation(check)*100,strrep(stats_sub.Target(check),'_',' '),'FontSize',6,'Color','blue');
    end
    % Draw negatives and label
    % Only draw negs with nonzero calc (>.01) to declutter
    check = ismember(stats_sub.Determination,{'Negative'}) & stats_sub.Calculation > 0.01;
    if find(check,1)
        scatter(log10(stats_sub.Depth_Mean(check)),stats_sub.Calculation(check)*100,36,'red','filled');
        hold on;
        text(log10(stats_sub.Depth_Mean(check))+0.1,stats_sub.Calculation(check)*100,strrep(stats_sub.Target(check),'_',' '),'FontSize',6,'Color','red');
    end
    % Draw indeterminates and label
    check = ismember(stats_sub.Determination,{'Indeterminate-Depth'}) | ismember(stats_sub.Determination,{'Indeterminate-Quality'});
    if find(check,1)
        scatter(log10(stats_sub.Depth_Mean(check))',stats_sub.Calculation(check)'*100,36,[1 0.65 0],'filled');
        hold on;
        text(log10(stats_sub.Depth_Mean(check))'+0.1,stats_sub.Calculation(check)'*100,strrep(stats_sub.Target(check),'_',' '),'FontSize',6,'Color',[1 0.65 0]); % color is orange
    end
    % Print graph to file
    print([pathToReads filesep 'graphs' filesep matlab.lang.makeValidName(stats_sub.Sample{1}) '.png'],'-dpng','-r300');
    % Delete data from axes
    h = gcf;
    delete(h.CurrentAxes.Children(1:end-7));
    if verbose == 2 && ishandle(wb)
        waitbar(i+length(ref)/(size(stats,1)+length(ref)));
    elseif verbose == 2 && ~ishandle(wb)
        return;
    end
end
close(h);
if verbose == 1
    fprintf('completed in %0.2f min\n',toc/60);
elseif verbose == 2
    close(wb);
end

% Reset incomplete table row addition warning
warning('on');

% Display analysis completion
msg = sprintf('%s [TARGNGS] Analysis complete\n',datestr(now));
fprintf(log_fi,msg);
if verbose == 1; fprintf(msg); end;

end % targetedNGS

%% AUXILIARY FUNCTIONS

function displayerror(msg,log_fi,verbose)
% Override default error() function to cater to verbosity style
if ~isempty(log_fi)
    fprintf(log_fi,'%s [FATLERR] %s\n',datestr(now),msg);
    fprintf(log_fi,'%s [FATLERR] Quitting now\n',datestr(now));
    fclose(log_fi);
end
if verbose == 1
    error(msg);
elseif verbose == 2
    errordlg(msg);
    error(msg);
end
end % displayerror

function [fileList,fileSizes] = getByExt(directory,extension,log_fi,verbose,exclude)
% Return list of all files in directory with extension
    % Error check input
    if ~ischar(extension)
        displayerror('Extension must be a string!',log_fi,verbose);
    end
    if isempty(extension)
        displayerror('No extension provided!',log_fi,verbose);
    end
    if nargin < 5
        exclude = '';
    end
    if ~ischar(exclude)
        displayerror('Exclusion must be a string!',log_fi,verbose);
    end

    % Return list of files matching extension
    fileList = dir(directory);
    fileSizes = [fileList.bytes]';
    fileSizes = fileSizes(3:end);
    fileList = {fileList.name}';
    fileList = fileList(3:end);
    count = 1;
    while count <= length(fileList)
        if length(fileList{count}) > length(extension) && isequal(fileList{count}(end-length(extension)+1:end),extension) && isempty(strfind(fileList{count},exclude))
            count = count + 1; % keep
        else
            fileList(count) = []; % remove name
            fileSizes(count) = []; % remove size
        end
    end
end % getByExt

function [coverage,mapq,match_baseq,mismatch_baseq,mismatch_seq] = getMappingData(bmap_sub,ref)
% Extract sequence and quality information from BioMap object
    % Get coverage and depth source
    coverage = getBaseCoverage(bmap_sub,1,length(ref.Sequence));
    % Get mapping qualities
    mapq = uint8(bmap_sub.MappingQuality);
    % Get sequence and base qualities
    % use uint8 to save on RAM (1 byte/# vs 8/# for double)
    mismatch_seq = strjoin(bioinfoprivate.cigar2gappedsequencemex(bmap_sub.Sequence,bmap_sub.Signature,false,true,true));
    match_baseq = strjoin(bioinfoprivate.cigar2gappedsequencemex(bmap_sub.Quality,bmap_sub.Signature,false,true,true));
    mismatch_baseq = int8(match_baseq(mismatch_seq~='=')) - 33;
    match_baseq = int8(match_baseq(mismatch_seq=='=')) - 33;
    mismatch_seq = mismatch_seq(mismatch_seq~='=');
end % getMappingData

%% CHANGELOG

% v1.0  (15DEC2016) initial release
% v1.1  (23JAN2017) improved read/CIGAR expansion to include deletions
% v1.2  (23JAN2017) improved calculations to include deletions
% v1.3  (23JAN2017) added detailed logging to file
% v1.4  (23JAN2017) added mate pair checking and orphan sequestering
% v1.5  (23JAN2017) improved empty file and results error handling